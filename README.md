##vagrant-ie
Based on the https://github.com/markhuber/modern-ie-vagrant and virtual machine images released with the [Modern.IE project](http://dev.modern.ie/), vagrant-ie makes testing IE easier with help from [Vagrant](http://vagrantup.com). The machine images in vagrant-ie are based on the original images with the addition of activating WinRM to make them compatible with the [Vagrant WinRM communicator](http://docs.vagrantup.com/v2/vagrantfile/winrm_settings.html).

##Prerequisites

vagrant-ie requires [Oracle Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://vagrantup.com) be installed and in your path.

##Usage

To list the available virtual machines

```bash
vagrant status
```

To start a new virtual machine

```bash
vagrant up IE10-Win7
```

To start multiple virtual machines at once

```bash
vagrant up IE11-Win7 IE10-Win7 IE9-Win7 IE8-Win7
```

##Available Vagrant Boxes

* IE10-Win7
* IE10-Win8
* IE11-Win10
* IE11-Win7 
* IE11-Win8.1
* IE6-WinXP
* IE7-Vista
* IE8-Win7
* IE8-WinXP
* IE9-Win7

#Login Instructions

Login Information (for Windows Vista, 7, 8, 10 VMs): IEUser, Passw0rd!
Instructions to set password for XP VMs:

* Using virtualization platform of choice, load the XP VM
* Go to Control Panel | User Accounts
* Select IEUser
* Select "Create a password" link and enter the desired password 